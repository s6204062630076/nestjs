import { Controller, Get } from '@nestjs/common';
import { StudentService } from 'src/student/services/student/student.service';
@Controller('student')
export class StudentController {
  constructor(private studentService: StudentService) {}
  @Get('Ex1')
  async load1(): Promise<[]> {
    return this.studentService.Ex1();
  }

  @Get('Ex2')
  async load2(): Promise<[]> {
    return this.studentService.Ex2();
  }

  @Get('Ex3')
  async load3(): Promise<[]> {
    return this.studentService.Ex3();
  }

  @Get('Ex4')
  async load4(): Promise<[]> {
    return this.studentService.Ex4();
  }

  @Get('Ex5')
  async load5(): Promise<[]> {
    return this.studentService.Ex5();
  }

  @Get('Ex6')
  async load6(): Promise<[]> {
    return this.studentService.Ex6();
  }

  @Get('Ex7')
  async load7(): Promise<[]> {
    return this.studentService.Ex7();
  }
}
