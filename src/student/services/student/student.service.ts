import { Injectable } from '@nestjs/common';
import { getManager } from 'typeorm';
@Injectable()
export class StudentService {
  async Ex1(): Promise<[]> {
    const entityManager = getManager();
    const Ex1 = await entityManager.query(
      `
  SELECT * FROM student
  ORDER BY std_name
  `,
      [],
    );
    return Ex1;
  }

  async Ex2(): Promise<[]> {
    const entityManager = getManager();
    const Ex2 = await entityManager.query(
      `
  SELECT std_id,std_name FROM student
  `,
      [],
    );
    return Ex2;
  }

  async Ex3(): Promise<[]> {
    const entityManager = getManager();
    const Ex3 = await entityManager.query(
      `
  SELECT * FROM student WHERE province = 'ขอนแก่น'
  `,
      [],
    );
    return Ex3;
  }

  async Ex4(): Promise<[]> {
    const entityManager = getManager();
    const Ex4 = await entityManager.query(
      `
  SELECT course.course_id,course.title,course.credit 
  FROM course 
  INNER JOIN register ON course.course_id = register.course_id
  WHERE register.std_id = '5001100348'
  `,
      [],
    );
    return Ex4;
  }

  async Ex5(): Promise<[]> {
    const entityManager = getManager();
    const Ex5 = await entityManager.query(
      `
  SELECT register.std_id,SUM(course.credit) credit
  FROM register 
  INNER JOIN course ON register.course_id = course.course_id
  GROUP BY register.std_id
  `,
      [],
    );
    return Ex5;
  }

  async Ex6(): Promise<[]> {
    const entityManager = getManager();
    const Ex6 = await entityManager.query(
      `
  SELECT course.title,COUNT(register.std_id) amount
  FROM course
  INNER JOIN register ON register.course_id = course.course_id
  GROUP BY course.title
  `,
      [],
    );
    return Ex6;
  }

  async Ex7(): Promise<[]> {
    const entityManager = getManager();
    const Ex7 = await entityManager.query(
      `
  SELECT student.std_name,register.course_id
  FROM student
  INNER JOIN register ON register.std_id = student.std_id
  WHERE register.course_id = '322236'
  `,
      [],
    );
    return Ex7;
  }
}
