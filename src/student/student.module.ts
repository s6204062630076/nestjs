import { Module } from '@nestjs/common';
// import { TypeOrmModule } from '@nestjs/typeorm';
import { StudentController } from './controller/student/student.controller';
// import { CourseEntity } from './entity/course.entity';
// import { RegisterEntity } from './entity/register.entity';
// import { StudentEntity } from './entity/student.entity';
import { StudentService } from './services/student/student.service';

@Module({
  // imports: [TypeOrmModule.forFeature([student, course, register])],
  controllers: [StudentController],
  providers: [StudentService],
})
export class StudentModule {}
