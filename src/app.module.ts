import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
// import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
// import { UserEntity } from './user/entity/user.entity';
// import { StudentEntity } from './student/entity/student.entity';
// import { RegisterEntity } from './student/entity/register.entity';
// import { CourseEntity } from './student/entity/course.entity';
import { StudentModule } from './student/student.module';
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: './DB/student.db',
      synchronize: true,
    }),
    StudentModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
