import { Injectable } from '@nestjs/common';
import { UserDto } from 'src/user/dto/user.dto/user-dto';
import { Repository } from 'typeorm';
import { UserEntity } from 'src/user/entity/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
@Injectable()
export class UserService {
  public users: UserDto[] = [];
  // create(user: UserDto): UserDto {
  //   this.users.push(user);
  //   return user;
  // }
  // loadAll(): UserDto[] {
  //   return this.users;
  // }
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {}
  create(user: UserDto): Promise<UserDto> {
    return this.userRepository.save(user);
  }
  loadAll(): Promise<UserDto[]> {
    return this.userRepository.find();
  }
  async remove(id: number): Promise<void> {
    await this.userRepository.delete(id);
  }
  async loadOne(id: number): Promise<UserDto> {
    return this.userRepository.findOne({ id });
  }
}
