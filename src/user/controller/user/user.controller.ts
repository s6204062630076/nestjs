import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { UserDto } from 'src/user/dto/user.dto/user-dto';
import { UserService } from 'src/user/services/user/user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}
  @Post()
  create(@Body() user: UserDto): Promise<UserDto> {
    // return 'This action adds a new user';
    // return this.userService.create(user);
    return this.userService.create(user);
  }
  @Get()
  async loadAll(): Promise<UserDto[]> {
    return this.userService.loadAll();
  }
  @Delete(':id')
  async delete(@Param('id') id: number): Promise<any> {
    await this.userService.remove(id);
    return { success: true };
  }
  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() udto: UserDto,
  ): Promise<UserDto> {
    const user = await this.userService.loadOne(id);
    user.name = udto.name;
    user.lastName = udto.lastName;
    return await this.userService.create(user);
  }
}
